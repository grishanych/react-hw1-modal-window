import { useState } from "react";
import Button from "./components/Button";
import FullScreenWrapper from "./components/FullScreenWrapper";
import Modal from "./components/Modal";
import styles from './components/styles/Modal.module.scss';
import './main.scss'




function App() {

  const [showModal, setShowModal] = useState(false);

  const openModal = () => {
    console.log('first btn')
    
    showModalSecond ? alert('Please close SECOND Modal') : setShowModal(true)
  }

  const cancelButton = (
    <button className={styles.modalButton} onClick={() => {
      alert('You really want to cloce this?')
      setShowModal(false)
    }}>Cancel</button>
  );

  const saveButton = (
    <button className={styles.modalButton} onClick={() => setShowModal(false)}>Ok</button>
  );

  const actionButtons = [saveButton, cancelButton];



  const [showModalSecond, setShowModalSecond] = useState(false);

  const openModalSecond = () => {
    console.log('second btn')
    showModal ? alert("Please close FIRST modal.") : setShowModalSecond(true)
  }


  const saveButtonOnSecModal = (
    <button className={styles.secondModalButton} onClick={() => setShowModalSecond(false)}>Save</button>
  );
  const cancelButtonOnSecModal = (
    <button className={styles.secondModalButton} onClick={() => {
      alert('You really want to cloce this?')
      setShowModalSecond(false)
    }}>Exit</button>
  );



  const actionButtonsForSecondModal = [saveButtonOnSecModal, cancelButtonOnSecModal];

  const handleCloseModal = (event) => {
    if ((showModal || showModalSecond) && event.target.closest('.modal')) {
      console.log(event.target)
      setShowModal(false);
      setShowModalSecond(false)
      
    }

  };

  

  return (
    <FullScreenWrapper heandler={handleCloseModal} showModal={showModal} showModalSecond={showModalSecond}>
      <Modal
        header='Do you want to delete this file?'
        text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
        actions={actionButtons}
        showModal={showModal}
        setShowModal={setShowModal}
        closeButton={() => setShowModal(false)}
      />

      <Modal
        header='This is a modal 2'
        text='Random text foe second Modal'
        actions={actionButtonsForSecondModal}
        showModal={showModalSecond}
        setShowModal={setShowModalSecond}
        closeButton={() => setShowModalSecond(false)}
      />
      <Button backgroundColor='lightblue' text='Open first modal' heandler={openModal} />
      <Button backgroundColor='lightgreen' text='Open second modal' heandler={openModalSecond} />
    </FullScreenWrapper>
  );
}

export default App;
