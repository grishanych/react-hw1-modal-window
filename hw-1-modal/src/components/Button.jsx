import styles from './styles/Buttons.module.scss'

function Button(props) {


    return (
        <button onClick={props.heandler}
            style={{ backgroundColor: props.backgroundColor }}
            className={styles.button}>{props.text}</button>

    )
}

export default Button