import styles from './styles/Wrapper.module.scss'

function FullScreenWrapper({ children, heandler, showModal, showModalSecond }) {

    const modalWrapperClass = `${styles.wrapper} ${showModal || showModalSecond ? styles.open : ''}`;

    return (
            <div onClick={heandler}
            className={modalWrapperClass}
            >{children}</div>
)
}

export default FullScreenWrapper